
public class query {
		
		//insert into the historical prices table
		public String insert_historical() {
			return "INSERT INTO HistoricalPrices (StockID, Date, Open, High, Low, Close, Volume) VALUES (?, STR_TO_DATE(?,'%Y-%m-%d'), ?, ?, ?, ?, ?)";
		}
		
		public String insert_realtime() {
			return "INSERT INTO RealtimePrices (StockID, Time, Open, High, Low, Close, Volume) VALUES (?, ?, ?, ?, ?, ?, ?)";
		}
		
		
		//get stockID from ticker
		public String get_stockID() {
		return "SELECT StockID FROM Stocks WHERE Ticker = ? LIMIT 1";	
		}
		
		
		public String update_price() {
			return "UPDATE Stocks SET Price = ?, Time = ? WHERE StockID = ?";
		}
		
		//insert new stock into stocks database
		public String insert_newStock() {
			return "INSERT INTO Stocks (Ticker, Company, Price, Volume, Time) VALUES (?,?,?,?,?)";
		}
		

	
}
