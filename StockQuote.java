import java.net.*;
import java.io.*;


public class StockQuote {
	private dbConnection connection;
	private query qr;
	
	public StockQuote()
	{
		connection=new dbConnection();
		qr=new query();
	}
	public void printStockQuote(String symbol){
	   String line;
       URL url = null;
       URLConnection urlConn = null;
       this.connection.connect(); //connect to the database

       try{
           url  = new              
               URL("https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="
               		+symbol+ "&interval=1min&apikey=GISI1EC15NO4ZLF3&datatype=csv");
           
           
           urlConn = url.openConnection();
       } catch(IOException ioe){
    	   ioe.printStackTrace();
       }
       
       try(InputStreamReader inStream = 
    		   new InputStreamReader(urlConn.getInputStream());

    	   BufferedReader buff  = new BufferedReader(inStream);){

           // get the quote as a csv string
           line =buff.readLine();  

           // parse the csv string
          while (line!=null)
          {
        	  System.out.println(line);
        	  line=buff.readLine();
          }
     } catch(MalformedURLException e){
         System.out.println("Please check the spelling of " 
                              + "the URL: " + e.toString() );
     } catch(IOException  e1){
      System.out.println("Can't read from the Internet: " + 
                                           e1.toString() ); 
     }
   }
	
	
	public String[] getInfor(String symbol)
	{
		String[] res= {};
		String line;
		URL url = null;
	    URLConnection urlConn = null;
	    this.connection.connect(); //connect to the database

	    try{
	        url  = new              
	               URL("https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="
	               		+symbol+ "&interval=1min&apikey=GISI1EC15NO4ZLF3&datatype=csv");
	           
	           
	        urlConn = url.openConnection();
	       } catch(IOException ioe){
	    	   ioe.printStackTrace();
	       }
	       
	       try(InputStreamReader inStream = 
	    	   new InputStreamReader(urlConn.getInputStream());

	    	   BufferedReader buff  = new BufferedReader(inStream);){

	           // get the quote as a csv string
	           line=buff.readLine(); 
	           
	           //read the second line of data which is the latest information
	           line=buff.readLine(); 
	           System.out.println(line);
	           res=line.split(",");

	           // parse the csv string
	           
	     } catch(MalformedURLException e){
	         System.out.println("Please check the spelling of " 
	                              + "the URL: " + e.toString() );
	     } catch(IOException  e1){
	      System.out.println("Can't read from the Internet: " + 
	                                           e1.toString() ); 
	     }
	       
	      return res;
	}
	

	

  public static void main(String args[]){
       if (args.length==0){
          System.out.println("Sample Usage: java StockQuote IBM");
          System.exit(0);
       } 
       StockQuote qt=new StockQuote();
       qt.printStockQuote(args[0]);
  }
}