import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class dbConnection {
	private Connection connection; //database connection
	private String dbURL="jdbc:mysql://localhost:3306/stockdb";
	private String username="root";
	private String password="root";
	
	public void connect() {
		try {
			this.connection=DriverManager.getConnection(dbURL,username,password);
			//System.out.println("Successful Connect");
			
		} catch(SQLException ex){
			ex.printStackTrace();
		}
	}
	
	public PreparedStatement prepare(String sql)
	{
		PreparedStatement statement=null;
		try {
			statement =this.connection.prepareStatement(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return statement;
		
	}
	
	public int executeInsert(PreparedStatement statement)
	{
		int row=-1;
		try {
			row=statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return row;
	}
	
	public ResultSet executeSelect(PreparedStatement statement)
	{
		ResultSet result=null;
		
		try {
			result=statement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public int executeUpdate(PreparedStatement statement)
	{
		int rows=-1;
		
		try {
			rows=statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rows;
	}

}
