import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;


public class Manager {
	private dbConnection connection;
	private query qr;
	
	
	public Manager()
	{
		connection=new dbConnection();
		qr=new query();
	}
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Map<String,String> stockLists =new HashMap<String,String>();  
		stockLists.put("MSFT","Microsoft Corporation");
		stockLists.put("INTC","Intel Corporation");
		stockLists.put("CSCO","Cisco Systems, Inc.");
		stockLists.put("ADBE","Adobe Inc.");
		stockLists.put("ORCL","Oracle Corporation");
		/*
		stockLists.put("NVDA","NVIDIA Corporation");
		stockLists.put("SAP","SAP SE");
		stockLists.put("IBM","International Business Machines Corporation");
		stockLists.put("SNE","Sony Corporation");
		stockLists.put("GOOG","Alphabet Inc.");
		*/
		Manager mng=new Manager();
		try {
			mng.create_database();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mng.connection.connect(); //The manager should connect to database once created
		
		
		
        for (Map.Entry mapElement : stockLists.entrySet()) { 
            String key = (String)mapElement.getKey(); 
            // Add some bonus marks 
            // to all the students and print it 
            mng.addStock((HashMap<String, String>)stockLists, key);

        }
        
       
		TimeUnit.MINUTES.sleep(1); //sleep one minute
		
        mng.add_Historical(stockLists);
 
        
        while (true)
        {
        	TimeUnit.MINUTES.sleep(1);
     
        	mng.update_All(stockLists);
       
        }
        

	}
	
	public void create_database() throws ClassNotFoundException, SQLException
	{
		String sql_createdatabase=null;
		String sql_createStocks=null;
		String sql_createHistorical=null;
		String sql_createRealtime=null;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(
					"setting.txt"));
			//set username
			String line = reader.readLine();
			this.connection.username=line;
			//set password
			line = reader.readLine();
			this.connection.password=line;
			//create database
			line = reader.readLine();
			sql_createdatabase=line;
			//create stocks
			line = reader.readLine();
			sql_createStocks=line;
			//create historical price
			line = reader.readLine();
			sql_createHistorical=line;
			//create realtime price
			line = reader.readLine();
			sql_createRealtime=line;
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	      //STEP 3: Open a connection
	      System.out.println("Connecting to database...");
	      Connection conn = DriverManager.getConnection(this.connection.url, this.connection.username, this.connection.password);
	      Statement stmt = conn.createStatement();
	      stmt.executeUpdate(sql_createdatabase);
	      System.out.println("Database created successfully...");
	      stmt.executeUpdate(sql_createStocks);
	      System.out.println("Stocks table created successfully...");
	      stmt.executeUpdate(sql_createHistorical);
	      System.out.println("HistoricalPrice table created successfully...");
	      stmt.executeUpdate(sql_createRealtime);
	      System.out.println("RealtimePrice table created successfully...");
	      conn.close();
	      
	      this.connection.dbURL=this.connection.url+"stockdb";
	      
	}
	
	public void add_Historical(Map<String, String> stockLists)
	{
		stockExtractor ext=new stockExtractor();

        for (Map.Entry mapElement : stockLists.entrySet()) { 
            String key = (String)mapElement.getKey(); 
            // Add some bonus marks 
            // to all the students and print it 
           ext.extractHistorical(key, this.get_stockID(key));
        }
	}
	
	public void update_All(Map<String, String> stockLists )
	{ 
		Iterator itr = stockLists.entrySet().iterator(); 
		while (itr.hasNext())
		{
			Map.Entry element=(Map.Entry)itr.next(); 
			System.out.println("Update "+element.getKey());
			this.update_Stock((String)element.getKey());
			
		}
	}
	
	public void update_Stock(String symbol)
	{
		StockQuote stockQuote=new StockQuote();
		String[] info=stockQuote.getInfor(symbol);
		PreparedStatement statement=this.connection.prepare(this.qr.update_price());
		PreparedStatement insert_realtime = this.connection.prepare(this.qr.insert_realtime());//insert into realtime
		try {
			statement.setDouble(1,Double.parseDouble(info[4]));//price
			statement.setTimestamp(2,Timestamp.valueOf(info[0]));//time
			statement.setInt(3,this.get_stockID(symbol)); //stockID
			this.connection.executeUpdate(statement);
			
			
			insert_realtime.setInt(1,this.get_stockID(symbol));//id
			insert_realtime.setTimestamp(2,Timestamp.valueOf(info[0]));//time
			insert_realtime.setDouble(3,Double.parseDouble(info[1]));//open
			insert_realtime.setDouble(4,Double.parseDouble(info[2]));//high
			insert_realtime.setDouble(5,Double.parseDouble(info[3]));//low
			insert_realtime.setDouble(6,Double.parseDouble(info[4]));//close
			insert_realtime.setInt(7,Integer.parseInt(info[5]));//volume
			this.connection.executeInsert(insert_realtime);
			
	
		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public int get_stockID(String symbol)
	{
		int ID=-1; //Initialize with -1
		PreparedStatement statement=this.connection.prepare(this.qr.get_stockID());
		try {
			statement.setString(1, symbol);
			ResultSet result=this.connection.executeSelect(statement);
			if (result==null)
			{
				return -1; //not found
			}
			result.next();
			ID=result.getInt("stockID");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ID;
	}
	
	public void addStock(HashMap<String,String>stockLists,String symbol)
	{

		StockQuote stockQuote=new StockQuote();
		String[] info=stockQuote.getInfor(symbol);
		if (info==null)
		{
			System.out.println("NULL");
		}
		PreparedStatement statement = this.connection.prepare(this.qr.insert_newStock());
		PreparedStatement insert_realtime = this.connection.prepare(this.qr.insert_realtime());
		if (statement==null)
		{
			System.out.println("Cannot Prepare SQL Statement");
			return;
		}
		
		try {
			statement.setString(1, symbol);  //ticker
			statement.setString(2, stockLists.get(symbol)); //company
			statement.setDouble(3, Double.parseDouble(info[4])); //price
			statement.setInt(4, Integer.parseInt(info[5])); //volume
			statement.setTimestamp(5,Timestamp.valueOf(info[0])); //Time
			this.connection.executeInsert(statement);
			
			
			insert_realtime.setInt(1,this.get_stockID(symbol));//id
			insert_realtime.setTimestamp(2,Timestamp.valueOf(info[0]));//time
			insert_realtime.setDouble(3,Double.parseDouble(info[1]));//open
			insert_realtime.setDouble(4,Double.parseDouble(info[2]));//high
			insert_realtime.setDouble(5,Double.parseDouble(info[3]));//low
			insert_realtime.setDouble(6,Double.parseDouble(info[4]));//close
			insert_realtime.setInt(7,Integer.parseInt(info[5]));//volume
			this.connection.executeInsert(insert_realtime);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //Ticker
		// add historical data after add the stock
		

	}

}
