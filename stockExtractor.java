import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class stockExtractor {
	private dbConnection connection;
	private query qr;
	
	public stockExtractor()
	{
		connection=new dbConnection();
		qr=new query();
	}
	
	public void extractHistorical(String symbol, Integer stockid)
	{
		   String line;
	       URL url = null;
	       URLConnection urlConn = null;
	       this.connection.connect(); //connect to the database

	       try{
	           url  = new              
	               URL("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="
	               		+symbol+ "&apikey=GISI1EC15NO4ZLF3&datatype=csv");
	           
	           urlConn = url.openConnection();
	       } catch(IOException ioe){
	    	   ioe.printStackTrace();
	       }
	       
	       try(InputStreamReader inStream = 
	    		   new InputStreamReader(urlConn.getInputStream());

	    	   BufferedReader buff  = new BufferedReader(inStream);){

	           // get the quote as a csv string
	           line =buff.readLine();
	           line =buff.readLine(); 

	           // parse the csv string
	          while (line!=null)
	          {
	        	 
	        	  
	        	  //System.out.println(line);
	        	  String[] values=line.split(",");
	        	  PreparedStatement statement = this.connection.prepare(this.qr.insert_historical());
					if (statement==null)
					{
						System.out.println("Cannot Prepare SQL Statement");
						return;
					}
					
					statement.setInt(1, stockid); //stockid is passed as parameter
					statement.setString(2, values[0]); //Date
					statement.setDouble(3, Double.parseDouble(values[1]));//open
					statement.setDouble(4, Double.parseDouble(values[2]));//high
					statement.setDouble(5, Double.parseDouble(values[3]));//low
					statement.setDouble(6, Double.parseDouble(values[4]));//close
					statement.setInt(7, Integer.parseInt(values[5])); //volume
					this.connection.executeInsert(statement);
					line=buff.readLine();
	        	  
	          }
	     } catch(MalformedURLException e){
	         System.out.println("Please check the spelling of " 
	                              + "the URL: " + e.toString() );
	     } catch(IOException  e1){
	      System.out.println("Can't read from the Internet: " + 
	                                           e1.toString() ); 
	     } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       
	       
			
			
		
	}
}
